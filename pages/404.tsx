import { Flex, Heading } from "@chakra-ui/react";
import { ReactNode } from "react";

export default function NotFoundPage<NextPage>() {
  return <></>;
}

NotFoundPage.getLayout = function (): ReactNode {
  return (
    <Flex w="100vw" h="100vh" justifyContent="center" alignItems="center">
      <Heading>Page Not Found</Heading>
    </Flex>
  );
};
