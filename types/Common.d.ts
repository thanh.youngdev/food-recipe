export interface CloudinaryResponse {
  secure_url: string;
  public_id: string;
}

export interface CommentInput {
  commentId: string;
  content: string;
  userId: string;
  publicId?: string[];
}

export interface Comment {
  _id: string;
  content: string;
  author?: string;
  withImages?:
    | {
        public_id: string;
        secure_url: string;
        name?: string;
      }[]
    | [];
  likes: string[];
  dislikes: string[];
  postId: string;
  postedBy: {
    user: {
      _id: string;
      username: string;
      email: string;
      personalImage: string;
    };
  };
  publicId?: [string];
  createdAt: string;
  updatedAt: string;
}
