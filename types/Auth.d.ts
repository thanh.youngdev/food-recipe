export interface SignInFormValues {
  username: string;
  email: string;
  password: string;
}
export interface RegisterFormValues extends SignInFormValues {
  confirmPassword?: string;
}

export interface ErrorForm extends RegisterFormValues {}

export interface User {
  _id?: string;
  type?: string;
  id: string;
  favoriteList?: [];

  // email: string;
  username: string;
  personalImage: string;
  accessToken: string;
}
